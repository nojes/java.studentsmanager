package org.itstep.controller;

import org.itstep.App;
import org.itstep.dao.pojo.Lesson;
import org.itstep.helper.Faker;
import org.itstep.service.LessonService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LessonControllerTest {

    @MockBean
    LessonService lessonService;

    @Autowired
    TestRestTemplate testRestTemplate;

    private Lesson lesson;

    @Before
    public void setUp() throws Exception {
        lesson = Faker.getFakeLesson();
    }

//    @After
//    public void tearDown() throws Exception {
//    }

    @Test
    public void createLesson() throws Exception {
        Mockito.when(lessonService.isUnique(Mockito.<Lesson>any())).thenReturn(true);
        Mockito.when(lessonService.saveAndUpdate(Mockito.<Lesson>any())).thenReturn(lesson);
        RequestEntity<Lesson> request = null;
        try {
            request = new RequestEntity<Lesson>(lesson, HttpMethod.POST, new URI("/lesson"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        ResponseEntity<Lesson> response = testRestTemplate.exchange(request, Lesson.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Mockito.verify(lessonService, Mockito.times(1)).saveAndUpdate(Mockito.<Lesson>any());
    }

    @Test
    public void updateLesson() throws Exception {
         createLesson();
    }

    @Test
    public void getOneByGroupAndStartTime() throws Exception {
        Mockito.when(lessonService.getOneByGroupAndStartTime(Mockito.anyString(), Mockito.anyLong())).thenReturn(lesson );
        RequestEntity<Lesson> request = null;
        String url = "/lesson/get-one-by-group-and-time?group="+lesson.getGroup()+"&startTime="+lesson.getLessonStart();
        try {
            request = new RequestEntity<Lesson>(lesson, HttpMethod.GET, new URI(url));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        ResponseEntity<Lesson> response = testRestTemplate.exchange(request, Lesson.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Mockito.verify(lessonService, Mockito.times(1))
                .getOneByGroupAndStartTime(Mockito.anyString(), Mockito.anyLong());
    }

    @Test
    public void getOneByTeacherAndStartTime() throws Exception {
        Mockito.when(lessonService.getOneByTeacherAndStartTime(Mockito.anyString(), Mockito.anyLong())).thenReturn(lesson );
        RequestEntity<Lesson> request = null;
        String url = "/lesson/get-one-by-teacher-and-time?teacher="+lesson.getTeacher()+"&startTime="+lesson.getLessonStart();
        try {
            request = new RequestEntity<Lesson>(lesson, HttpMethod.GET, new URI(url));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        ResponseEntity<Lesson> response = testRestTemplate.exchange(request, Lesson.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Mockito.verify(lessonService, Mockito.times(1))
                .getOneByTeacherAndStartTime(Mockito.anyString(), Mockito.anyLong());
    }

}