package org.itstep.helper;

import org.itstep.dao.pojo.Lesson;

import java.sql.Timestamp;

public class Faker {

    public static Lesson getFakeLesson() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Lesson lesson = new Lesson();

        lesson.setGroup("G7");
        lesson.setLength((long)7);
        lesson.setLessonId((long)777777);
        lesson.setLessonStart(timestamp.getTime());
        lesson.setRoom("R7");
        lesson.setSubject("Subject7");
        lesson.setTeacher("Teacher7");

        return lesson;
    }
}
